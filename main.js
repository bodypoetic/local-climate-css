(() => {
    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again,
     * it will do nothing next time.
     */
    if (window.hasRun) { return }
    window.hasRun = true;


    // check if can get location
    if (!navigator.geolocation) {
        console.error( "Cannot get geolocation" )
        return
    }


    // how often to check
    const CHECK_INT = 5000


    // CSS transition parameters
    const WIND_TRANS_DUR = 5000
    const WIND_TRANS_FUN = 'ease'
    const HUMI_TRANS_DUR = 1000
    const HUMI_TRANS_FUN = 'linear'


    // weather api parameters
    const CLIMATE_API_KEY = 'f74916e1c9606ec06e53c7c0d82b6674'


    // setup style tag
    let style = set_up_weather_styles_tag()

    // run
    run()

    // every few seconds
    setInterval(() => run() , CHECK_INT )


    // run
    function run() {
        // get location latitude and longitutde from browser
        navigator.geolocation.getCurrentPosition( position => {
            const lat = position.coords.latitude
            const lon = position.coords.longitude
            // construct api URL with current lat and lon info
            const api_url = `https://api.openweathermap.org/data/2.5/weather?lat=${ lat }&lon=${ lon }&appid=${ CLIMATE_API_KEY }`
            // fetch weather data
            fetch( api_url )
                .then( response => response.json() )
                .then( data => set_weather_css( data ) )
                .catch( err => console.error( err ) )
        })
    }

    // set up styles tag
    function set_up_weather_styles_tag() {
        const style = document.createElement( 'style' )
        style.id = "weather_style"
        style.setAttribute( 'contenteditable', true )
        document.body.appendChild( style )
        return style
    }


    // set wind data
    function set_weather_css( data ) {
        console.log( data )
        const wind = blow( data.wind.speed )
        const humidity = moisten( data.main.humidity )
        const temp = temperature(data.main.temp)
        const sunriseset = light(data.sys.sunset, data.sys.sunrise, data.dt)

        style.innerHTML = `
            /* wind */
            ${ wind }
            /* humidity */
            ${ humidity }
            /* temperature */
            ${ temp }
            /* cloudiness */
            /* precipitation */
            /* sunrise or sunset */
            ${ sunriseset }
        `
    }


    // function to blow website
    function blow( wind ) {
        console.log(`Blowing at local windspeed ${ wind }m/s.`)
        wind /= 20
        return `
            body * {
                transform: rotate(${ Math.random() * wind - wind / 2 }deg);
                transition: transform ${ WIND_TRANS_DUR }ms ${ WIND_TRANS_FUN };
            }
        `
    }

    // function to handle humidity
    function moisten( humidity ) {
        console.log(`Setting humidity to ${ humidity }%.`)
        humidity *= 0.01
        return `
            body::after{
                /* overlay something above everything */
                position: fixed;
                inset: 0;
                content: "";
                pointer-events: none;
                z-index: 999999;

                /* make the edges white and center visible */
                background: radial-gradient(closest-side, transparent ${ 100 * humidity }%, white);

                /* make everything below blurry - value can be modified */
                backdrop-filter: blur(${ humidity }px);
                /* but still show a bit of the original non-blured - value can be modified */
                opacity: ${ humidity };
                transition: all ${ HUMI_TRANS_DUR }ms ${ HUMI_TRANS_FUN };
            }
        `
    }

    // temperature
    function temperature( temperature ) {
        temperature -= 273.15
        console.log( `Setting temperature to ${ temperature }℃.` )
        if ( temperature > 10 ) {
            return `
                /* melting (for above a certain level of temperature) */
                p {
                    --melting-strongness: ${ temperature };
                    position: relative;
                    transform-origin: top center;
                    animation: melt 5s linear infinite;
                }
                @keyframes melt{
                    0%{top: 0; opacity: 0; transform: scaleY(1);}
                    10%{top: 0; opacity: 1; transform: scaleY(1);}
                    55%{top: calc(0.1em * var(--melting-strongness)); opacity: 1; transform: scaleY(calc(1 + (0.05 * var(--melting-strongness))));}
                    100%{top: calc(0.2em * var(--melting-strongness)); opacity: 0; transform: scaleY(calc(1 + (0.1 * var(--melting-strongness))));}
                }
            `
        } else {
            return ``
        }

    }

    // sunrise or sunset
    function light(sunset, sunrise, currenttime) {

        var timetosunset = sunset - currenttime
        var timetosunrise = sunrise - currenttime

        console.log(`Time to sunset:` + timetosunset)
        console.log(`Time to sunrise:` + timetosunrise)

            
        

        if ((timetosunset <= 3600 && timetosunset > 0) || (timetosunrise <= 3600 && timetosunrise > 0) || (timetosunset >= -3600 && timetosunset < 0) || (timetosunrise >= -3600 && timetosunrise < 0))  {
            return `
            img {
                filter: sepia(%);
            }`
        }

        if ((timetosunset <= 1800 && timetosunset > 0) || (timetosunrise <= 1800 && timetosunrise > 0) || (timetosunset >= -1800 && timetosunset < 0) || (timetosunrise >= -1800 && timetosunrise < 0)) {
            return `
            img {
                filter: sepia (66%);
            }`
        }

        if ((timetosunset <= 600 && timetosunset > 0) || (timetosunrise <= 600 && timetosunrise > 0) || (timetosunset >= -600 && timetosunset < 0) || (timetosunrise >= -600 && timetosunrise < 0)) {
            return `
            img {
                filter: sepia (100%);
            }`
        }
    }






    /**
     * Listen to popup message
     */
    browser.runtime.onMessage.addListener((message) => {

        // if it get that message!
        if (message.command === "blow") {
            blow( message.value );
        }

    });

})();
