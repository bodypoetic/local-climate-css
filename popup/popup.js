

function download(tabs) {
  const scale = document.getElementById('blow_scale').value
  browser.tabs.sendMessage(tabs[0].id, {
    command: "blow",
    value: scale
  });
}

function listenForClicks() {
  document.addEventListener("click", (e) => {
    if (e.target.tagName !== "BUTTON" || !e.target.closest("#popup-content")) {
      return;
    }
    else {
      browser.tabs
        .query({ active: true, currentWindow: true })
        .then(download)
        .catch(err => console.error(err));
    }
  })
}

browser.tabs
  .executeScript({file: "../main.js"})
  .then(listenForClicks)
  .catch(err => console.error(err));
